<?php

use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('studies')->insert([
            'code' => 'IFC301',
            'name' => 'Desarrollo De Aplicaciones Web',
            'family_id'=> '1'
        ]);
        DB::table('studies')->insert([
            'code' => 'IFC302',
            'name' => 'Desarrollo De Aplicaciones Multiplataforma',
            'family_id'=> '1'
        ]);
        DB::table('studies')->insert([
            'code' => 'ADG301',
            'name' => 'Administracion y Finanzas',
            'family_id'=> '2'
        ]);
        DB::table('studies')->insert([
            'code' => 'IMP301',
            'name' => 'Asesoria de Imagen Personal',
            'family_id'=> '3'
        ]);
    }
}
