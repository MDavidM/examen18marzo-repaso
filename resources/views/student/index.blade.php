@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de estudiantes</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Fecha</th>
            <th>Dirección</th>
            <th>Email</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($students as $student)
          <tr>
            <td>{{ $student->name }}</td>
            <td>{{ $student->lastname }}</td>
            <td>{{ $student->date }}</td>
            <td>{{ $student->address }}</td>
            <td>{{ $student->email }}</td>
            <td>
              <form method="post" action="/students/{{ $student->id }}">
                @can ('update', $student)
                @endcan
                <a class="btn btn-primary"  role="button"
                href="/students/{{ $student->id }}">
                  Ver
                </a>
                <a class="btn btn-info"  role="button"
                href="/students/{{ $student->id }}/edit">
                  Editar
                </a>
              <!--{{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              @can ('delete', $student)
                <input type="submit" value="borrar" class="btn btn-danger">
              @endcan-->
            </form>
          </td>
          <td>
            <form method="post" action="/students/{{ $student->id }}">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="delete">
              <input type="submit" value="borrar" class="btn btn-danger">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay estudiantes!!</td></tr>
        @endforelse
      </tbody>
      <a class="btn btn-primary" role="button" href="/students/create">Nuevo</a>
    </table>
    {{ $students->render() }}
  </div>
</div>
</div>
@endsection
