@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Edición de estudiante</h1>
      <form class="form" method="post" action="/students/{{ $student->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" class="form-control" name="name" value="{{ $student->name}}">
            @if ($errors->first('name'))
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
            @endif
        </div>
         <div class="form-group">
            <label>Apellido</label>
            <input type="text" class="form-control" name="lastname" value="{{ $student->lastname }}">
            @if ($errors->first('lastname'))
            <div class="alert alert-danger">
                {{ $errors->first('lastname') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Fecha</label>
            <input type="text" class="form-control" name="date" value="{{ $student->date}}">
            @if ($errors->first('date'))
            <div class="alert alert-danger">
                {{ $errors->first('date') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Dirección</label>
            <input type="text" class="form-control" name="address" value="{{ $student->address}}">
            @if ($errors->first('address'))
            <div class="alert alert-danger">
                {{ $errors->first('address') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{ $student->email }}">
            @if ($errors->first('email'))
            <div class="alert alert-danger">
                {{ $errors->first('email') }}
            </div>
            @endif
        </div>
        <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
