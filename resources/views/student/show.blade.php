@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>Datos del estudiante</h1>
      <ul>
        <li>Nombre: {{ $student->name }}</li>
        <li>Apellido: {{ $student->code }}</li>
        <li>Fecha: {{ $student->date }}</li>
        <li>Dirección: {{ $student->address }}</li>
        <li>Email: {{ $student->email }}</li>
      </ul>
    </div>
  </div>
</div>
@endsection
