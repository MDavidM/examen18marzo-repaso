@extends('layouts.app')

@section('content')
<h1 class="alert alert-info">creacion de estudios</h1>
<div class="container">
    <form class="form" method="post" action="/studies">
        {{csrf_field()}}
        <div class="form-group">
            <label>nombre</label>
            <input class="form-control" type="text" name="name" value="{{old('name')}}">

                @if( $errors->first('name'))
            <script>
                alert('el campo nonbre es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('name')}}
            </div>
            @endif

        </div>
        <div class="form-group">
        <label>codigo</label>
        <input class="form-control" type="text" name="code" value="{{old('code')}}">

            @if( $errors->first('code'))
            <script>
                alert('el campo codigo es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('code')}}
            </div>
            @endif
    </div>

    <div class="form-group">
            <label>familia</label>
            <select class="form-control" type="text" name="family_id">
                <option></option>
                @foreach($familys as $family)
                <option value="{{$family->id}}" {{old('family_id')==$family->id ? 'selected="selected"' : ''}}>{{$family->name}}</option>
                @endforeach
            </select>

            @if( $errors->first('family_id'))
            <script>
                alert('el campo modulo es obligatorio');
            </script>

            <div class="alert alert-danger">{{$errors->first('family_id')}}
            </div>
            @endif
            {{-- <input type="submit" name="siguiente" class="btn btn-primary"> --}}

        </div>
        <input class="btn btn-primary" type="submit" name="nuevo" value="nuevo">
    </form>
</div>
@endsection
