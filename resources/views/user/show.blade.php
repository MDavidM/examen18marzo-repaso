@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>Datos del usuario</h1>
      <ul>
        <li>{{ $user->name }}</li>
        <li>{{ $user->email }}</li>
      </ul>
      <h2>Lista de exámenes del usuario </h2>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Titulo
            </th>
            <th>
              Fecha
            </th>
            <th>
              Módulo
            </th>
          </tr>
        </thead>
        <tbody>
          @forelse($user->exams as $exam)
          <tr>
            <td>
              {{ $exam->title }}
            </td>
            <td>
              {{ $exam->date }}
            </td>
            <td>
              {{ $exam->module->name }}
            </td>
          </tr>
          @empty
          <tr>
            <td>No hay exámenes</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
