@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de usuarios</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
              <form method="post" action="/users/{{ $user->id }}">
                @can ('update', $user)
                @endcan

                <a class="btn btn-primary"  role="button"
                href="/users/{{ $user->id }}">
                  Ver
                </a>
              <!--{{ csrf_field() }}-->
              <input type="hidden" name="_method" value="DELETE">
              @can ('delete', $user)
                <input type="submit" value="borrar" class="btn btn-danger">
              @endcan
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>
    {{ $users->render() }}
  </div>
</div>
</div>
@endsection
