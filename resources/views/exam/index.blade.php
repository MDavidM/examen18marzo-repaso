@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de exámenes</h1>
      @if (Session::has('exam'))
      Examen recordado: {{ Session::get('exam')->title }}
      <a class="btn btn-warning" href="/exams/forget">Olvidarlo
      </a>
      @endif
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Título</th>
            <th>Fecha</th>
            <th>Módulo</th>
            <th>Usuario</th>
            <th>Acciones</th>
            <th>Borrar</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($exams as $exam)
          <tr>
            <td>{{ $exam->title }}</td>
            <td>{{ $exam->date }}</td>
            <td>{{ $exam->module->name }}</td>
            <td>{{ $exam->user->name }}</td>
            <td><a class="glyphicon glyphicon-eye-open" href="/exams/{{$exam->id }}"></a> <a class="glyphicon glyphicon-pencil" href="/exams/{{$exam->id }}/edit"></a> <a class=" glyphicon glyphicon-plus" href="/exams/{{$exam->id }}/remember"></a> </td>
            <td>
              @can('delete', $exam)
              <form method="post" action="/exams/{{$exam->id}}">
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="delete">
                  <input type="submit" value="borrar {{$exam->id}}" class="glyphicon glyphicon-trash">
              </form>
              @endcan
            </td>
          @endforeach
          </tr>
              <!--<td><form method="post" action="/exams/{{ $exam->id }}">
                can ('update', $exam)
                endcan
                <a class="btn btn-primary"  role="button"
                href="/exams/{{ $exam->id }}">
                  Ver
                </a>
                <a class="btn btn-info"  role="button"
                href="/exams/{{ $exam->id }}/edit">
                  Editar
                </a>
                 <a class="btn btn-info"  role="button"
                href="/exams/{{ $exam->id }}/remember">
                  Recordar
                </a>
              { csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              can ('delete', $exam)
                <input type="submit" value="borrar" class="btn btn-danger">
              endcan
            </form>-->
          </td>
        </tr>
<!--empty
        <tr><td colspan="4">No hay exámenes!!</td></tr>
        endforelse-->
      </tbody>
    </table>
    {{ $exams->render() }}
  </div>
</div>
<a class="btn btn-primary" role="button" href="/exams/create">Nuevo</a>
</div>
@endsection
