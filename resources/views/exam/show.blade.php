@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>Datos del exámen</h1>
      <ul>
        <li>Título: {{ $exam->title }}</li>
        <li>Fecha: {{ $exam->date }}</li>
        <li>Usuario: {{ $exam->user->name }}</li>
        <li>Módulo: {{ $exam->module->name }}</li>
      </ul>
      <h2>Lista de preguntas del exámen</h2>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Texto
            </th>
            <th>
              A
            </th>
            <th>
              B
            </th>
            <th>
              C
            </th>
            <th>
              D
            </th>
             <th>
              Respuesta
            </th>
          </tr>
        </thead>
        <tbody>
        @foreach($exam->questions as $question){{--  --}}
        <tr>
        <td>{{$question->text}}</td>
        <td class="{{$question->answer == 'a' ? 'bg-success' : ''}}">{{$question->a}}</td>
        <td class="{{$question->answer == 'b' ? 'bg-success' : ''}}">{{$question->b}}</td>
        <td class="{{$question->answer == 'c' ? 'bg-success' : ''}}">{{$question->c}}</td>
        <td class="{{$question->answer == 'd' ? 'bg-success' : ''}}">{{$question->d}}</td>
        <td >{{$question->answer}}</td>
         @endforeach
        </tr>
          <!--@forelse($exam->questions as $question)
          <tr>
            <td>
              {{ $question->text }}
            </td>
            <td>
              {{ $question->a }}
            </td>
            <td>
              {{ $question->b }}
            </td>
            <td>
              {{ $question->c }}
            </td>
            <td>
              {{ $question->d }}
            </td>
             <td>
              {{ $question->answer }}
            </td>
          </tr>
          @empty
          <tr>
            <td>No hay exámenes</td>
          </tr>
          @endforelse-->
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
