@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Edición de examen</h1>
      <form class="form" method="post" action="/exams/{{ $exam->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="form-group">
            <label>Título</label>
            <input type="text" class="form-control" name="title" value="{{ $exam->title }}">
            @if ($errors->first('title'))
            <div class="alert alert-danger">
                {{ $errors->first('title') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Fecha</label>
            <input type="date" class="form-control" name="date" value="{{ $exam->date}}">
            @if ($errors->first('date'))
            <div class="alert alert-danger">
                {{ $errors->first('date') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Módulo</label>
            <select class="form-control" type="text" name="module_id">
                <option></option>
                @foreach ($modules as $module)
                <option value="{{ $module->id }}" {{ $exam->module_id == $module->id ? '
                selected="selected"' : '' }}>
                {{ $module->name}}
                </option>
                @endforeach
            </select>
            @if ($errors->first('module_id'))
            <div class="alert alert-danger">
                {{ $errors->first('module_id') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Usuario</label>
            <select class="form-control" type="text" name="user_id">
                <option></option>
                @foreach ($users as $user)
                <option value="{{ $user->id }}" {{ $exam->user_id == $user->id ? '
                selected="selected"' : '' }}>
                {{ $user->name}}
                </option>
                @endforeach
            </select>
            @if ($errors->first('user_id'))
            <div class="alert alert-danger">
                {{ $errors->first('user_id') }}
            </div>
            @endif
        </div>
        <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
