@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Alta de examen</h1>
      <form class="form" method="post" action="/exams">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Título</label>
            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
            @if ($errors->first('title'))
            <script>
                alert('el campo titulo es obligatorio');
            </script>
            <div class="alert alert-danger">
                {{ $errors->first('title') }}
            </div>
            @endif
        </div>
        <div class="form-group">
          <label>Fecha</label>
          <input type="date" class="form-control" name="date" value="{{ old('date') }}">
          @if ($errors->first('date'))
          <script>
                alert('el campo fecha es obligatorio');
            </script>
          <div class="alert alert-danger">
            {{ $errors->first('date') }}
          </div>
          @endif
        </div>
        <div  class="form-group">
          <label>Módulo</label>
          <select  class="form-control"  name="module_id" value="{{ old('date') }}">
          <option></option>
          @foreach ($modules as $module)
{{--
            <option value="{{ $module->id}}">{{ $module->name }}</option>
 --}}
          <option value="{{ $module->id}}"
            {{ old('module_id') == $module->id ? 'selected="selected"' : '' }}>
            {{ $module->name }}
          </option>
          @endforeach
          </select>
            @if ($errors->first('module_id'))
            <script>
                alert('el campo modulo es obligatorio');
            </script>
            <div class="alert alert-danger">
              {{ $errors->first('module_id') }}
          </div>
            @endif
            <input type="submit" class="btn btn-primary" role="button">
      </form>
    </div>
  </div>
</div>
@endsection
