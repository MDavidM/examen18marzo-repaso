@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>Datos de la pregunta</h1>
      <ul>
        <li>Texto: {{ $question->text }}</li>
        <li>A: {{ $question->a }}</li>
        <li>B: {{ $question->b }}</li>
        <li>C: {{ $question->c }}</li>
        <li>D: {{ $question->d }}</li>
        <li>Respuesta: {{ $question->answer }}</li>
        <li>Módulo: {{ $question->module_id }}</li>
      </ul>
      <h2>Lista de exámenes asociados </h2>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Titulo
            </th>
            <th>
              Módulo
            </th>
          </tr>
        </thead>
        <tbody>
          @forelse($question->exams as $exam)
          <tr>
            <td>
              {{ $exam->title }}
            </td>
            <td>
              {{ $exam->module->name }}
            </td>
          </tr>
          @empty
          <tr>
            <td>No hay exámenes</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
