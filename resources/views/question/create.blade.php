@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Alta de pregunta</h1>
      <form class="form" method="post" action="/questions">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Texto</label>
            <input type="text" class="form-control" name="text" value="{{ old('text') }}">
            @if ($errors->first('text'))
            <div class="alert alert-danger">
                {{ $errors->first('text') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>A</label>
            <input type="text" class="form-control" name="a" value="{{ old('a') }}">
            @if ($errors->first('a'))
            <div class="alert alert-danger">
                {{ $errors->first('a') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>B</label>
            <input type="text" class="form-control" name="b" value="{{ old('b') }}">
            @if ($errors->first('b'))
            <div class="alert alert-danger">
                {{ $errors->first('b') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>C</label>
            <input type="text" class="form-control" name="c" value="{{ old('c') }}">
            @if ($errors->first('c'))
            <div class="alert alert-danger">
                {{ $errors->first('c') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>D</label>
            <input type="text" class="form-control" name="d" value="{{ old('d') }}">
            @if ($errors->first('d'))
            <div class="alert alert-danger">
                {{ $errors->first('d') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Respuesta</label>
            <input type="text" class="form-control" name="answer" value="{{ old('answer') }}">
            @if ($errors->first('answer'))
            <div class="alert alert-danger">
                {{ $errors->first('answer') }}
            </div>
            @endif
        </div>
         <div class="form-group">
            <label>Módulo</label>
            <select class="form-control" type="text" name="module_id">
                <option></option>
                @foreach ($modules as $module)
                <option value="{{ $module->id }}">{{ $module->name}}</option>
                @endforeach
            </select>
            @if ($errors->first('module_id'))
            <div class="alert alert-danger">
                {{ $errors->first('module_id') }}
            </div>
            @endif
        </div>
         <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
