@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de preguntas</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Texto</th>
            <th>Módulo</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($questions as $question)
          <tr>
            <td>{{ $question->text }}</td>
            <td>{{ $question->module->name }}</td>
            <td>
              <form method="post" action="/users/{{ $question->id }}">
                @can ('update', $question)
                @endcan
                <a class="btn btn-primary"  role="button"
                href="/questions/{{ $question->id }}">
                  Ver
                </a>
                <a class="btn btn-info"  role="button"
                href="/questions/{{ $question->id }}/edit">
                  Editar
                </a>
              <!--{{ csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              @can ('delete', $question)
                <input type="submit" value="borrar" class="btn btn-danger">
              @endcan-->
            </form>
          </td>
          <td>
            <form method="post" action="/questions/{{ $question->id }}">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="delete">
              <input type="submit" value="borrar" class="btn btn-danger">
            </form>
          </td>
        </tr>
        @empty
        <tr><td colspan="4">No hay preguntas!!</td></tr>
        @endforelse
      </tbody>
      <a class="btn btn-primary" role="button" href="/questions/create">Nuevo</a>
    </table>
    {{ $questions->render() }}
  </div>
</div>
</div>
@endsection
