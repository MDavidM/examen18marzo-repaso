@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>Datos del módulo</h1>
      <ul>
        <li>Nombre: {{ $module->name }}</li>
        <li>Código: {{ $module->code }}</li>
      </ul>
      <h2>Lista de exámenes del módulo </h2>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              Titulo
            </th>
            <th>
              Fecha
            </th>
          </tr>
        </thead>
        <tbody>
          @forelse($module->exams as $exam)
          <tr>
            <td>
              {{ $exam->title }}
            </td>
            <td>
              {{ $exam->date }}
            </td>
          </tr>
          @empty
          <tr>
            <td>No hay exámenes</td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
