@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de modulos</h1>
      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Código</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($modules as $module)
          <tr>
            <td>{{ $module->name }}</td>
            <td>{{ $module->code }}</td>
            <td>

                <a class="glyphicon glyphicon-eye-open" href="/modules/{{$module->id }}"></a>
      @can('view' ,$module)

      @endcan
        <a class="glyphicon glyphicon-pencil" href="/modules/{{$module->id }}/edit"></a>

      <form method="post" action="/modules/{{$module->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="delete">
        <input type="submit" value="borrar {{$module->id}}" class="btn btn-danger">
      </form>
    </td>
              <!--<form method="post" action="/modules/{{ $module->id }}">
                can ('update', $module)
                endcan
                <a class="btn btn-primary"  role="button"
                href="/modules/{{ $module->id }}">
                  Ver
                </a>
                <a class="btn btn-info"  role="button"
                href="/modules/{{ $module->id }}/edit">
                  Editar
                </a>
              { csrf_field() }}
              <input type="hidden" name="_method" value="DELETE">
              can ('delete', $module)
                <input type="submit" value="borrar" class="btn btn-danger">
            endcan
            </form>
          </td>
          <td>
            <form method="post" action="/modules/{{ $module->id }}">
            { csrf_field() }}
              <input type="hidden" name="_method" value="delete">
              <input type="submit" value="borrar" class="btn btn-danger">
            </form>
          </td>-->
        </tr>
        <!--empty
        <tr><td colspan="4">No hay modulos!!</td></tr>
        endforelse-->
         @endforeach
      </tbody>
      <a class="btn btn-primary" role="button" href="/modules/create">Nuevo</a>
    </table>
    {{ $modules->render() }}
  </div>
</div>
</div>
@endsection
