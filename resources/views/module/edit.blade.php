@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Edición de módulo</h1>
      <form class="form" method="post" action="/modules/{{ $module->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" class="form-control" name="name" value="{{ $module->name}}">
            @if ($errors->first('name'))
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
            @endif
        </div>
         <div class="form-group">
            <label>Código</label>
            <input type="text" class="form-control" name="code" value="{{ $module->code }}">
            @if ($errors->first('code'))
            <div class="alert alert-danger">
                {{ $errors->first('code') }}
            </div>
            @endif
        </div>
        <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
