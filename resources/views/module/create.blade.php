@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Alta de módulo</h1>
      <form class="form" method="post" action="/modules">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
            @if ($errors->first('name'))
              <script>
                alert('el campo nonbre es obligatorio');
            </script>
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
            @endif
        </div>
        <div class="form-group">
            <label>Código</label>
            <input type="text" class="form-control" name="code" value="{{ old('name') }}">
            @if ($errors->first('code'))
             <script>
                alert('el campo codigo es obligatorio');
            </script>
            <div class="alert alert-danger">
                {{ $errors->first('code') }}
            </div>
            @endif
        </div>
         <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
