<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $students = Student::paginate(15);

        return view('student.index', ['students'=>$students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name'=>'required|max:100',
            'lastname'=>'required|max:200',
            'date'=>'date|required',
            'address'=>'required|max:255',
            'email'=>'required|unique:students,email|max:100',
        ];

        $request->validate($rules);


        $student =  new Student();
        $student->fill($request->all());
        $student->save();
        return redirect('/students');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $student = Student::findOrFail($id);

        return view('student.show', ['student'=>$student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
         $student = Student::findOrFail($id);

        return view('student.edit', ['student' => $student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $rules =[
            'name'=>'required|max:100',
            'lastname'=>'required|max:200',
            'date'=>'date|required',
            'address'=>'required|max:255',
            'email' => 'required|unique:students,email,' . $id . '|max:100'
        ];
        $request->validate($rules);

        $student = Student::findOrFail($id);
        $student->fill($request->all());
        $student->save();

        return redirect("/students/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Student::destroy($id);
        return back();
    }
}
