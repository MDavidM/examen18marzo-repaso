<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam;
use App\Question;
use App\Module;
use App\User;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Exam::all();
        return Exam::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //no hace falta en api
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255|unique:exams,title',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }

        $exam = new Exam;
        $exam->fill($request->all());
        $exam->save();

        return $exam;


        /*$rules = [
            'title' => 'required|max:255|unique:exams,title',
            'date' => 'required|date',
            'user_id' => 'exists:users,id',
            'module_id' => 'exists:modules,id'
        ];
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            //return $validator->errors(); esta da status 200. mejor el de abajo
            return response()->json($validator->errors(),400);
        }

        $request->validate($rules);
        $exam = new Exam;
        $exam->fill($request->all());
        //$exam->user_id = \Auth::user()->id; no nos hace falta en api porque no esta logueado
        $exam->save();
        //return redirect('/exams'); no tiene sentido en este api
        return $exam;*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::with('user', 'module', 'questions')->find($id);
        if($exam) {
            return $exam;
        } else {
            return response()->json([
                'message' => 'Record not found'
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //no hace falta en api
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|unique:exams,title,' . $id . '|max:255',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];
        $validator = Validator::make($request->all(),$rules);

         if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }

        $exam = Exam::with('user', 'module')->find($id);
        if(!$exam) {
            return response()->json([
                'message' => 'Record not found'
            ],404);
        }
        $exam->fill($request->all());
        $exam->save();
        $exam->refresh();
        return $exam;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::destroy($id);
        return response()->json([
            'message' => 'deleted'
            ],402);
    }
}
