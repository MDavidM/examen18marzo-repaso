<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Module;
use App\Question;
use App\Exam;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Module::with('exams')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //no hace falta en api
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:modules,name',
            'code' => 'required|max:255|unique:modules,code'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }
        $module = new Module;
        $module->fill($request->all());
        //$module->user_id = \Auth::user()->id; no nos hace falta en api porque no esta logueado
        $module->save();

        //return redirect('/nidykes'); no tiene sentido en este api
        return $module;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return Module::with('exams', 'questions')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //no hace falta en api
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module = Module::find($id);
        $rules = [
            'name' => 'required|unique:modules,name,' . $id . '|max:255',
            'code' => 'required|unique:modules,code,' . $id . 'max:255'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }

        if(!$module) {
            return response()->json([
                'message' => 'Record not found'
            ],404);
        }
        $module->fill($request->all());
        $module->save();
        $module->refresh();
        return $module;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        return response()->json([
            'message' => 'deleted'
            ],402);
    }
}
