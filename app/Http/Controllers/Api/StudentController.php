<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Student::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name'=>'required|max:100',
            'lastname'=>'required|max:200',
            'date'=>'date|required',
            'address'=>'required|max:255',
            'email'=>'required|unique:students,email|max:100',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }
        $student =  new Student();
        $student->fill($request->all());
        $student->save();

        //return redirect('/nidykes'); no tiene sentido en este api
        return $student;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $exam = Exam::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module = Module::find($id);
        $rules =[
            'name'=>'required|max:100',
            'lastname'=>'required|max:200',
            'date'=>'date|required',
            'address'=>'required|max:255',
            'email' => 'required|unique:students,email,' . $id . '|max:100'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);
        }

        if(!$student) {
            return response()->json([
                'message' => 'Record not found'
            ],404);
        }

        $student->fill($request->all());
        $student->save();
        $student->refresh();
        return $student;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);
        return response()->json([
            'message' => 'deleted'
            ],402);
    }
}
