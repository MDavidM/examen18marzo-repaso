<?php

namespace App\Http\Controllers;

use App\Question;
use App\Exam;
use App\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $modules = Module::paginate();
        return view('module.index', ['modules' => $modules]);
        //return Module::with('exams')->get();

        /* // $this->authorize('index', Module::class); //hacer peticion sobre el modelo modules
        $user = \Auth::user();
        $modules = Module::paginate(5);
        if($user->can('index', Module::class)){
            return view('module.index', ['modules'=>$modules]);
        }else{
            //return view('errors.403');
        }


       // return view('module.index', ['modules'=>$modules]);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //$modules = Module::all();
        //return view('module.create', ['modules' => $modules]);
        return view('module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|unique:modules,name',
            'code' => 'required|max:255|unique:modules,code'
        ];
        $request->validate($rules);
        $module = new Module;
        $module->fill($request->all());
        //$module->user_id = \Auth::user()->id;
        $module->save();

        return redirect('/modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        /*$module = Module::findOrFail($id);
        return view('module.show', ['module' => $module]);
        //return Module::with('exams', 'questions')->find($id);*/
        $module = Module::findOrFail($id);
        $this->authorize('view', $module);

        return view('module.show', ['module'=>$module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $module = Module::findOrFail($id);

        return view('module.edit', ['module' => $module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:modules,name,' . $id . '|max:255',
            'code' => 'required|unique:modules,code,' . $id . '|max:255'
        ];
        $request->validate($rules);
        $module = Module::findOrFail($id);
        $module->fill($request->all());
        $module->save();

        return redirect("/modules/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        return back();
    }
}
