<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Module;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $questions = Question::paginate();
        return view('question.index', ['questions' => $questions]);
        //return Question::with('exams')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $modules = Module::all();
        return view('question.create', ['modules' => $modules]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $rules = [
            'text' => 'required|max:255|unique:questions,text',
            'a' => 'required|max:255',
            'b' => 'required|max:255',
            'c' => 'required|max:255',
            'd' => 'required|max:255',
            'answer' => 'required|max:255',
            'module_id' => 'exists:modules,id'
        ];
        $request->validate($rules);
        $question = new Question;
        $question->fill($request->all());
        //$question->user_id = \Auth::user()->id;
        $question->save();

        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $question = Question::findOrFail($id);
        return view('question.show', ['question' => $question]);
        //return Question::with('exams', 'module')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $modules = Module::all();

        return view('question.edit', [
            'question' => $question,
            'modules' => $modules
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'text' => 'required|unique:questions,text,' . $id . '|max:255',
            'a' => 'required|max:255',
            'b' => 'required|max:255',
            'c' => 'required|max:255',
            'd' => 'required|max:255',
            'answer' => 'required|max:255',
            'module_id' => 'exists:modules,id'
        ];
        $request->validate($rules);
        $question = Question::findOrFail($id);
        $question->fill($request->all());
        $question->save();

        return redirect("/questions/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Question::destroy($id);
        return back();
    }
}
