<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $fillable = ['name', 'code', 'family_id'];
    public function family()
    {
        return $this->belongsTo(Family::class);
    }

     public function modules()
    {
        return $this->belongsToMany(Module::class)->withPivot('course');
    }
}
