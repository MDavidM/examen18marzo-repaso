<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $fillable=['name','code' ];
    public function studies()
 {
    return $this->hasMany(Study::class);
}
}
