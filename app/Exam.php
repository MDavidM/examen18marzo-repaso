<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

    protected $fillable = ['title', 'module_id', 'user_id', 'date'];
    protected $dates = ['date'];

    public function user()
    {
        return $this->belongsTo('App\User');
        //return $this->belongsTo(User::class);
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
        //return $this->belongsTo(Module::class);
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question')->withPivot('order');
        //return $this->belongsToMany(Question::class)->withPivot('order');
    }
}
